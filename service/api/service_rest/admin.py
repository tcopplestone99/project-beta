from django.contrib import admin
from .models import Tech, Appointment

@admin.register(Tech)
class TechAdmin(admin.ModelAdmin):
    pass

@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    pass