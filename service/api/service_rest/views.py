from termios import VINTR
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder

from .models import Appointment, Tech, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "vip"]

class TechEncoder(ModelEncoder):
    model = Tech
    properties = ["name", "employee_id", "id"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["id","customer", "date", "time", "tech", "reason", "vin"]
    encoders = {
        "vin": AutomobileVOEncoder(),
        "tech": TechEncoder(),
    }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = ["id", "customer", "date", "time", "tech", "reason", "vin"]
    encoders = {
        "vin": AutomobileVOEncoder(),
        "tech": TechEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentListEncoder
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            # auto_href = content["vin"]
            # automobile = AutomobileVO.objects.get(import_href=auto_href)
            # content["vin"] = automobile
            
            vin = content["vin"]
            vin = AutomobileVO.objects.get(vin=vin)
            content["vin"] = vin
            
         
            tech_name = content["tech"]
            tech = Tech.objects.get(name=tech_name)
            content["tech"] = tech
        
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid submission"},
                status=400
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            {"appointment":appointment}, encoder=AppointmentDetailEncoder, safe=False
        )
    else:
        count, _= Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted":count > 0})
    

@require_http_methods(["GET"])
def api_service_history(request, vin):
    if request.method == "GET":
        # data = Appointment.objects.get(vin=vin.vin)
        # print(data)
        history = {"hist": []}
        for i in Appointment.objects.all():
            if i.vin.vin == vin:
                history["hist"].append(i)
        # history = Appointment.objects.get(vin.vin=vin)
        # print(history)
        return JsonResponse(
            history, encoder=AppointmentListEncoder, safe=False
        )
        
        
@require_http_methods(["GET", "POST"])
def api_tech_list(request):
    if request.method == "GET":
        tech = Tech.objects.all()
        return JsonResponse(
            {"tech": tech}, encoder=TechEncoder
        )
    else:
        content = json.loads(request.body)
        tech = Tech.objects.create(**content)
        return JsonResponse(
            tech,
            encoder=TechEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_inhouse_auto_list(request):
    if request.method == "GET":
        inhouse_autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"inhouse_autos": inhouse_autos}, encoder=AutomobileVOEncoder
        )

