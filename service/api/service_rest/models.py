from enum import unique
from django.db import models
from django.urls import reverse
from enum import unique


# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    vip = models.BooleanField(default=True)
    
    def __str__(self):
        return self.vin


class Tech(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.SmallIntegerField()
    
    def __str__(self):
        return self.name
    
    
class Appointment(models.Model):
    customer = models.CharField(max_length=100)
    date = models.DateField(auto_now=False)
    time = models.TimeField(auto_now=False)
    tech = models.ForeignKey(Tech, related_name="appointments", on_delete=models.CASCADE)
    reason = models.TextField(max_length=1000)
    vin = models.ForeignKey(AutomobileVO, related_name="appointments", on_delete=models.CASCADE)
    
    def __str__(self):
        return self.vin
    
    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})
    
    