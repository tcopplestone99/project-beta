from django.urls import path

from .views import(
    api_inhouse_auto_list,
    api_list_appointments,
    api_service_history,
    api_show_appointment,
    api_tech_list,
)

urlpatterns = [
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    path("service-history/<str:vin>/", api_service_history, name="api_service_history"),
    path("techs/", api_tech_list, name="api_tech_list"),
    path("inhouse-autos/", api_inhouse_auto_list, name="api_inhouse_auto_list")
    
]
