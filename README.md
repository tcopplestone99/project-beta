## CarCar - Project Beta
___
​
### Team:
* Angelo Lavo: Service
* Trewin Copplestone: Sales
___
​
### __Introduction__
___
​
CarCar is an application solution for total car dealership management. It addresses information management needs of modern dealerships and is based on the following business domains: inventory, sales and service. It is engineered for use by patrons as well as staff.
​
CarCar is a full-stack suite of micro-services built on a Django back-end framework utilizing API endpoints with a React UI. Each micro-service houses its own PostgreSQL database and the git repository is packaged for easy Docker deployment.
​
Guided by the Domain Driven Design approach to software engineering, each micro-service correlates to the business domains mentioned above, addressing the needs of each through bounded contexts and aggregates.
___
​
### Design
___
​
#### __Bounded Contexts & Models__
​
CarCar maintains three bounded contexts reflective of an automotive dealership's business domains. Inventory, a domain unto itself, shares automobile information with both sales and service.
​
#### Django Models
*Inventory* models include Automobile, VehicleModel and Manufacturer. Manufacturer is a ForeignKey to VehicleModel which is a ForeignKey to Automobile. The purpose of this interconnecting of models follows the logic of one-to-many relationships. Each  manufacturer has many models and produces even more automobiles. For each automobile in inventory there is only one model and manufacturer.
​
Inventoried automobiles are integral to both sales and service. Sales move the inventory and service tracks sold inventory, especially if the dealership offers a service package with a sale. For these reasons both the sales and service micro-services utilize the Automobile model as a value object (AutomobileVO). A value object is immutable so neither sales or service make changes to automobiles, that is strictly in the inventory domain. A poller is implemented to keep the sales and service databases updated  with each micro-service pulling and tracking them by VIN.
​
*Sales* models include SalesPerson, Customer, AutomobileVO, and SalesRecord. SalesRecord is an aggregate of all the sales models and each model is a foreign key to the sales record.
​
*Service* models include Tech (for technician), AutomobileVO and Appointment. As with the SalesRecord model, the Appointment model is an aggregate of the micro-service's models. AutomobileVO model has an added property 'vip' to track VIP status, this however does not alter the original model housed in inventory.
​
#### Django Views
Django views handling client-side requests utilize a RESTApi framework for CRUD processes. A list of API urls associated with each view are located in the technical appendix below. Each views file contains JSON encoders capable of returning serialized responses across all API endpoints for seamless data transfers. Views facilitate each application feature listed below and are referenced in italics. Both sales and service implement a view solely for accessing AutomobileVO but do so for different purposes. Service uses the *api_inhouse_auto* view for determining VIP status on the front-end. Sales implements *api_automobile_vo_list* when creating a record of sale.
​
#### __React Front-end__
​
All features below are implemented via React class components. Forms get or post data through handleSubmit or componentDidMount methods. There is only one class component for each file.
​
App-wide navigation consists of three drop-downs (Sales, Service and Inventory) in the navigation bar next to the Home link. Users can reach any of the features via the navigation bar. Next steps in development include implementing Single Page Application methods where practical.
​
** **Incomplete Feature Notification ** **
The Appointments List in Services is incomplete, still working on a solution for hiding individual rows once an appointment is completed.
​
___
#### __Features__
___
​
#### Sales
*Forms*
- add customer to database (name, address and phone number) *//api_customers*
- add salesperson to database (name and employee number) *//api_sales_persons*
- add record of sale to database (customer, VIN and sale price) *//api_sales_records*
​
*Lists*
* sales records by sales staff (customer, VIN and sale price) *//api_sales_person_details*

#### Service
​
*Forms*
- create new service appointment (customer name, date, time, reason for appointment, assigned technician and VIN) //*api_list_appointments*
- search service appointment history by VIN *//api_service_history*
- create new technician record (name, employee number) *//api_tech_list*
​
*Lists*
* upcoming service appointments with option to delete appointments (VIN, date, time reason, technician and VIP status (indicates whether a vehicle was purchased from dealership)) *//api_list_appointments* & *//api_show_appointment* (for deleting individual appointment)
​
#### Inventory
*Forms*
- add manufacturer to database (name) *//api_manufacturers*
- add model to database (name, photo and manufacturer) *//api_vehicle_models*
- add vehicle to inventory (color, make, model, year and vehicle identification number (VIN)) *//api_automobiles*
​
*Lists*
* vehicles currently in stock (color, make, model, year and VIN) *//api_automobiles*
* manufacturers *//api_manufacturers*
* models (name, picture, manufacturer) *//api_vehicle_models*
___
### Technical Appendix
___
Git repository:
[Link](https://gitlab.com/tcopplestone99/project-beta)
​
​
Installation:
 - Fork and copy clone link for repository in GitLab
 - Clone into local directory
 - Open directory in code editor
 - In editor terminal run following commands:
	 + ```
		docker volume create beta-data
		docker-compose build
		docker-compose up
		```
 - You should see the following containers "spin-up" on your Docker Desktop application:
​
![Image](./docker_compose_up.png)
​
Local Testing
To get started go to localhost:3000 in your browser and begin interacting with the application.
​
Functional dependencies exist within inventory, in order to create an automobile there must exist a correlative manufacturer and model. This means if you are going to add a vehicle you should first create the manufacturer (if one does not already exist) and then the model before creating the automobile through the form.
​
After you have created inventory you will be able to generate sales records and service appointments in the respective micro-services. And thereafter you will be able to generate lists, sales histories and service histories.
​
- File identification:
![Image](./file_identification.png)
​
Diagrams:
- Context map
![Image](./context_map.png)

API url endpoints and requisite JSON bodies (if applicable):
```
**SALES**
POST an GET requests:
Create customer
localhost:8090/api/customers/
{
	"name": "InsomTest",
	"address": "InsomTest",
	"phone_number": "ooga"
}
​
Create salesperson
localhost:8090/api/salespersons/
{
	"name": "Test",
	"employee_number": "9999"
}
​
Create sales record
localhost:8090/api/salesrecords/
{
	"price": "1",
	"customer": "1",
	"sales_person": "1",
	"car": "TestVin"
}
:
Automobiles list
localhost:8090/api/automobileVOs/
​
Sales person detail
localhost:8090/api/salespersons/<int:pk>
​
```
​
```
**SERVICE**
GETs or DELETES:
List appointments
http://localhost:8080/api/appointments/
​
Appointment details and Delete appointment
http://localhost:8080/api/appointments/5/ (GET & DELETE appointments)
​
List technicians
http://localhost:8080/api/techs/
​
List automobiles inventory
http://localhost:8080/api/inhouse-autos/
​
Service history by VIN
VINs currently in database (for testing in browser or insomnia)
"SADNESS333"
"B2MBB2MB"
"FG09876"
http://localhost:8080/api/service-history/SADNESS333/
​
POSTs:
Create appointment
http://localhost:8080/api/appointments/
{
	"vin": "SADNESS333",
	"customer": "the dude 4",
	"date": "2022-11-04",
	"time": "13:45:00",
	"tech": "Lazy Jay",
	"reason": "more stuff"
}
​
Create technician
http://localhost:8080/api/tech/
{
			"name": "Drooly Sully",
			"employee_id": "3"
}
```
​
```
AUTOMOBILES
Automobiles list
http://localhost:8100/api/automobiles/
​
Create automobile
http://localhost:8100/api/automobiles/
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
​
Automobile detail
http://localhost:8100/api/automobiles/SADNESS333/
​
Automobile delete
http://localhost:8100/api/automobiles/:vin/
​
Automobile update
http://localhost:8100/api/automobiles/SADNESS333/
{
  "color": "red",
  "year": 2012
}
​
VEHICLE MODELS
List models
http://localhost:8100/api/models/
​
Create model
http://localhost:8100/api/models/
{
  "name": "Lugg",
  "picture_url": "",
  "manufacturer_id": 1
}
Update model
http://localhost:8100/api/models/2/
{
  "name": "Lugg",
  "picture_url": "",
  "manufacturer_id": 1
}
​
Model detail
http://localhost:8100/api/models/2/
​
Delete model
http://localhost:8100/api/models/:id/
​
MANUFACTURERS
List manufacturers
http://localhost:8100/api/manufacturers/
​
Create manufacturer
http://localhost:8100/api/manufacturers/
{
  "name": "Ayooo"
}
​
Manufacturer detail
http://localhost:8100/api/manufacturers/1/
​
Update manufacturer
http://localhost:8100/api/manufacturers/1/
{
  "name": "Ayooo"
}
​
Delete manufacturer
http://localhost:8100/api/manufacturers/:id/
```
​
Please feel free to reach out to the team with any questions or suggestions.
Angelo Lavo: beforenever@yahoo.com
Trewin Copplestone: tcopplestone@gmail.com
