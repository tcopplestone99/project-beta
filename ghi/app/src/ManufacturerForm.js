import React from "react";

class ManufacturerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            successful: false
        }
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    renderSuccess() {
        if (this.state.successful) {
            return(
                <p>You have successfully created a manufacturer</p>
            )
        }
    }

    handleNameChange(event) {
        const value = event.target.value
        this.setState({name: value})
        // console.log(this.state.name)
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.successful
        const url = "http://localhost:8100/api/manufacturers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const r = await fetch(url, fetchConfig)
        if (r.ok) {
            const newMan = await r.json()
            const wipe = {
                name: "",
                successful: true
            }
            this.setState(wipe)
        }

    }

    render() {
        return (
            <div>
                <h1>Create a new manufacturer</h1>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" value={this.state.name}/>
                    </div>
                    <button>Create</button>
                </form>
                {this.renderSuccess()}
            </div>
        )
    }
}


export default ManufacturerForm
