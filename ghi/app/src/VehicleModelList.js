import React from "react";

class VehicleModelList extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            models: []
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/models/"
        const r = await fetch(url)
        if (r.ok) {
            const data = await r.json()
            // console.log(data)
            this.setState({models: data.models})
        }


    }
    render() {
        return (
            <div className="container">
                <h1>Car Models</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Picture</th>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map(m => {
                            return (
                                <tr key={m.id}>
                                    <td>{m.name}</td>
                                    <td>
                                        <img style={{ width:100 }} src={m.picture_url} alt=""></img>
                                    </td>
                                    <td>{m.manufacturer.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default VehicleModelList
