import React from "react"

class AddTechForm extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            name: "",
            employeeId: "",
            techCreated: false,
        }
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleEmployeeIdChange = this.handleEmployeeIdChange.bind(this)
        this.handleSubmit = this.handeSubmit.bind(this)
    }

    handleNameChange(event){
        const value = event.target.value
        this.setState({name: value})
    }

    handleEmployeeIdChange(event){
        const value = event.target.value
        this.setState({employeeId: value})
    }

    async handeSubmit(event){
        event.preventDefault()
        const data = {...this.state}

        data.employee_id = data.employeeId
        delete data.employeeId
        delete data.techCreated

        const techUrl = "http://localhost:8080/api/techs/"
        const fetchConfig = {
            method: "post",
            body:JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        };
        const response = await fetch(techUrl, fetchConfig);
        if(response.ok){
            const newTech = await response.json();
            // console.log(newTech)

            const cleared = {
                name: "",
                employeeId: "",
                techCreated: true,
            }
            this.setState(cleared)
        }

    }

    render(){
        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.techCreated) {
          messageClasses = 'alert alert-success mb-0';
          formClasses = 'd-none';
        }
        return(
            <>
            <div className="my-5 container">
                <div className="row">
                    <div className="col col-md-auto">
                        <div className="col">
                            <div className="card shadow">
                                <div className="card-body">
                                    <h1 className="card-title">Create Technician</h1>
                                    <form className={formClasses} onSubmit={this.handleSubmit} id="create-technician-form">
                                        <div className="mb-2">
                                            <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                            <label htmlFor="name">Name</label>
                                        </div>
                                        <div className="mb-2">
                                            <input onChange={this.handleEmployeeIdChange} value={this.state.employeeId} placeholder="Employee ID" required type="number" name="employee_id" id="employee_id" className="form-control" />
                                            <label htmlFor="employee_id">Employee ID</label>
                                        </div>
                                        <button>Create Technician</button>
                                    </form>
                                    <div className={messageClasses} id="success-message">
                                    New techician added!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </>
        )
    }


}
export default AddTechForm;
