import React from "react"


class AutomobileForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            color: "",
            year: "",
            vin: "",
            model_id: "",
            models: [],
            successful: false
        }
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handleYearChange = this.handleYearChange.bind(this)
        this.handleVinChange = this.handleVinChange.bind(this)
        this.handleModelChange = this.handleModelChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        // this.renderSuccess = this.renderSuccess.bind(this)
    }

    handleColorChange(event) {
        const value = event.target.value
        this.setState({color: value})

    }

    handleYearChange(event) {
        const value = event.target.value
        this.setState({year: value})
    }

    handleVinChange(event) {
        const value = event.target.value
        this.setState({vin: value})
    }

    handleModelChange(event) {
        const value = event.target.value
        this.setState({model_id: value})
    }

    renderSuccess() {
        if (this.state.successful) {
            return(
                <p>You have successfully created an automobile</p>
            )
        }
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.models
        delete data.successful
        // console.log(data)
        const url = "http://localhost:8100/api/automobiles/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const r = await fetch(url, fetchConfig)
        if(r.ok) {
            const newAuto = await r.json()
            const wipe = {
                color: "",
                year: "",
                vin: "",
                model_id: "",
                successful: true
            }
            this.setState(wipe)
        }
    }

    async componentDidMount() {
        const modUrl = "http://localhost:8100/api/models"
        const response = await fetch(modUrl)
        if (response.ok) {
            const modData = await response.json()
            this.setState({models: modData.models})
        }
    }


    render() {
        return (
            <div>
                <h1>Create a new automobile</h1>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" value={this.state.color} />
                    </div>
                    <div>
                        <input onChange={this.handleYearChange} placeholder="Year" required type="number" name="year" id="year" value={this.state.year} />
                    </div>
                    <div>
                        <input onChange={this.handleVinChange} placeholder="Vin" required type="text" name="vin" id="vin" value={this.state.vin} />
                    </div>
                    <div>
                        <select onChange={this.handleModelChange} required id ="model_id" name="model_id" value={this.state.model_id}>
                            <option value="">Choose a model</option>
                            {this.state.models.map(m => {
                                return (
                                    <option key={m.id} value={m.id}>{m.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <button>Create</button>
                </form>
                <div>
                    {this.renderSuccess()}
                </div>
            </div>
        )
    }

}

export default AutomobileForm
