import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AddTechForm from './AddTechForm';
import AppointmentForm from './AppointmentForm';
import AppointmentsList from './AppointmentsList';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './CustomerForm';
import EmployeeForm from './EmployeeForm';
import SalesRecordForm from './SalesRecordForm';
import SalesRecordList from './SalesRecordsList';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import VehicleServiceHistory from './VehicleServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/customers/new" element={<CustomerForm />}/>
          <Route path="/salespersons/new" element={<EmployeeForm />}/>
          <Route path="/salesrecords" element={<SalesRecordList />}/>
          <Route path="/salesrecords/new" element={<SalesRecordForm />}/>
          <Route path="/serviceappointments" element={<AppointmentsList />}/>
          <Route path="/serviceappointments/new/" element={<AppointmentForm />}/>
          <Route path="/servicehistory" element={<VehicleServiceHistory />}/>
          <Route path="/technician/new/" element={<AddTechForm />}/>
          <Route path="/manufacturers" element={<ManufacturerList />}/>
          <Route path="/manufacturers/new" element={<ManufacturerForm />}/>
          <Route path="/vehiclemodels" element={<VehicleModelList />}/>
          <Route path="/vehiclemodels/new" element={<VehicleModelForm />}/>
          <Route path="/automobiles" element={<AutomobileList />}/>
          <Route path="/automobiles/new" element={<AutomobileForm />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
