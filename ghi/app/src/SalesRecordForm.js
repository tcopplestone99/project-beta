import React from "react"

class SalesRecordForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            price: "",
            customer: "",
            sales_person: "",
            car: "",
            customers: [],
            sales_persons: [],
            cars: [],
            sales_records: [],
            successful: false
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleCarChange = this.handleCarChange.bind(this)
        this.handlePriceChange = this.handlePriceChange.bind(this)
        this.handleCustomerChange = this.handleCustomerChange.bind(this)
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this)

    }

    renderSuccess() {
        if (this.state.successful) {
            return(
                <p>You have successfully created a sales record</p>
            )
        }
    }

    handlePriceChange(event) {
        const value = event.target.value
        this.setState({price: value})
    }

    handleCustomerChange(event) {
        const value = event.target.value
        this.setState({customer: value})
    }

    handleSalesPersonChange(event) {
        const value = event.target.value
        this.setState({sales_person: value})
    }

    handleCarChange(event) {
        const value = event.target.value
        this.setState({car: value})
    }



    unsoldCars(data) {
        const unsoldCars = []
        // console.log(data)
        const vinRecs = this.state.sales_records.map(item => {return item.vin})
        for (let car of data.cars) {
            if (!vinRecs.includes(car.vin)) {
                unsoldCars.push(car)
            }
        }
        this.setState({cars: unsoldCars})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.cars
        delete data.customers
        delete data.sales_persons
        delete data.sales_records
        delete data.successful
        const SalesRecUrl = "http://localhost:8090/api/salesrecords/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const r = await fetch(SalesRecUrl, fetchConfig)
        if(r.ok) {
            const newSale = await r.json()
            const wipe = {
                car: "",
                price: "",
                sales_person: "",
                customer: "",
                successful: true
            }
            this.setState(wipe)
            this.componentDidMount()
        }
    }

    async componentDidMount() {
        const custUrl = "http://localhost:8090/api/customers/"
        const salePerUrl = "http://localhost:8090/api/salespersons/"
        const carUrl = "http://localhost:8090/api/automobileVOs/"
        const salesRecUrl = "http://localhost:8090/api/salesrecords/"
        const responseCust = await fetch(custUrl)
        if (responseCust.ok) {
            const custData = await responseCust.json()
            this.setState({customers: custData.customers})
        }
        const responseSalesPer = await fetch(salePerUrl)
        if (responseSalesPer.ok) {
            const salesPerData = await responseSalesPer.json()
            this.setState({sales_persons: salesPerData.sales_persons})
        }
        const responseSalesRec = await fetch(salesRecUrl)
        if (responseSalesRec.ok) {
            const salesRecData = await responseSalesRec.json()
            this.setState({sales_records: salesRecData.sales_records})
        }
        const responseCar = await fetch(carUrl)
        if (responseCar.ok) {
            const carData = await responseCar.json()
            this.unsoldCars(carData)
            // this.setState({cars: carData.cars})
        }

    }

    render() {
        return (
            <div>
                <h1>Buy a car any car</h1>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <select onChange={this.handleCarChange} required id="car" name="car" value={this.state.car}>
                            <option value="">Choose an automobile</option>
                            {this.state.cars.map(c => {
                                return (
                                    <option key={c.id} value={c.vin}>{c.vin}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div>
                        <select onChange={this.handleSalesPersonChange} required id="sales_person" name="sales_person" value={this.state.sales_person}>
                            <option value="">Choose a sales person</option>
                            {this.state.sales_persons.map(sp => {
                                return (
                                    <option key={sp.id} value={sp.id}>{sp.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div>
                        <select onChange={this.handleCustomerChange} required id="customer" name="customer" value={this.state.customer}>
                            <option value="">Choose a customer</option>
                            {this.state.customers.map(cu => {
                                return (
                                    <option key={cu.id} value={cu.id}>{cu.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div>
                        <input onChange={this.handlePriceChange} placeholder="Price" required type="number" name="price" id="price" value={this.state.price}/>
                    </div>
                    <button>BUY BUY BUY</button>
                </form>
                {this.renderSuccess()}
            </div>
        )
    }





}

export default SalesRecordForm
