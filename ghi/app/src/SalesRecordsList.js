import React from "react"

class SalesRecordList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            employee: "",
            employees: [],
            sales_records: []
        }
        this.handleEmployeeChange = this.handleEmployeeChange.bind(this)
    }


    handleEmployeeChange(event) {
        const value = event.target.value
        this.setState({employee: value})
    }



    showFiltered(id) {
        if (id == "") {
            let result = this.state.sales_records.map(rec => {
                return (
                    <tr key={rec.id}>
                        <td>{rec.sales_person.name}</td>
                        <td>{rec.customer.name}</td>
                        <td>{rec.vin}</td>
                        <td>${rec.price}</td>
                    </tr>
                )
            })
            return result
        } else {
            const filtered = this.state.sales_records.filter(rec => rec.sales_person.id == id)
            let result = filtered.map(rec =>{
                return (
                    <tr key={rec.id}>
                        <td>{rec.sales_person.name}</td>
                        <td>{rec.customer.name}</td>
                        <td>{rec.vin}</td>
                        <td>${rec.price}</td>
                    </tr>
                )
            })
            return result
        }
    }

    async componentDidMount() {
        const salesRecUrl = "http://localhost:8090/api/salesrecords/"
        const salePerUrl = "http://localhost:8090/api/salespersons/"
        const responseSalesRec = await fetch(salesRecUrl)
        if (responseSalesRec.ok) {
            const salesRecData = await responseSalesRec.json()
            this.setState({sales_records: salesRecData.sales_records})
        }
        const responseSalesPer = await fetch(salePerUrl)
        if (responseSalesPer.ok) {
            const salesPerData = await responseSalesPer.json()
            this.setState({employees: salesPerData.sales_persons})
        }
    }




    render() {
        return (
            <div>
                <h1>Sales Records</h1>
                <div>
                    <select onChange={this.handleEmployeeChange} required id="employee" name="employee" value={this.state.employee}>
                        <option value="">Select a sales person</option>
                        {this.state.employees.map(emp => {
                            return (
                                <option key={emp.id} value={emp.id}>{emp.name}</option>
                            )
                        })}
                    </select>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale Price $</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.showFiltered(this.state.employee)}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default SalesRecordList
