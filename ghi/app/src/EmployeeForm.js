import React from "react"


class EmployeeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            employee_number: "",
            successful: false
        }
        this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this)
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    renderSuccess() {
        if (this.state.successful) {
            return(
                <p>You have successfully created a sales person</p>
            )
        }
    }

    handleNameChange(event) {
        const value = event.target.value
        this.setState({name: value})
        // console.log(this.state)
    }

    handleEmployeeNumberChange(event) {
        const value = event.target.value
        this.setState({employee_number: value})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.successful
        // console.log(data)
        const custUrl = "http://localhost:8090/api/salespersons/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const r = await fetch(custUrl, fetchConfig)
        if(r.ok) {
            const newCust = await r.json()
            // console.log(newCust)
            const wipe = {
                name: "",
                employee_number: "",
                successful: true
            }
            this.setState(wipe)
            // console.log(this.state)
        }
    }

    render() {
        return (
            <div>
                <h1>Create a new sales person</h1>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <input onChange={this.handleNameChange} className="form-control" value={this.state.name} placeholder="Name" required type="text" name="name" id="name"/>
                        {/* <label  htmlFor="name">Name</label> */}
                    </div>
                    <div className="form-group">
                        <input onChange={this.handleEmployeeNumberChange} className="form-control" value={this.state.employee_number} placeholder="Employee Number" required type="text" name="employeeNumber" id="employeeNumber"/>
                        {/* <label htmlFor="employeeNumber">Employee Number</label> */}
                    </div>
                    <button>Create</button>
                </form>
                {this.renderSuccess()}
            </div>
        )
    }




}


export default EmployeeForm
