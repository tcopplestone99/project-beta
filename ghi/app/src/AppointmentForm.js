import React from "react"

class AppointmentForm extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            customer: "",
            date: "",
            time: "",
            reason: "",
            tech: "",
            techs: [],
            vin: "",
            vins: [],
            apptScheduled: false,

        }
           // handles
        this.handleCustomerChange = this.handleCustomerChange.bind(this)
        this.handleDateChange = this.handleDateChange.bind(this)
        this.handleTimeChange = this.handleTimeChange.bind(this)
        this.handleReasonChange = this.handleReasonChange.bind(this)
        this.handleTechChange = this.handleTechChange.bind(this)
        this.handleVinChange = this.handleVinChange.bind(this)
        // this.handleVipChange = this.handleVipChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }

    handleCustomerChange(event) {
        const value = event.target.value
        this.setState({customer: value})
    }

    handleDateChange(event) {
        const value = event.target.value
        this.setState({date: value})
    }

    handleTimeChange(event) {
        const value = event.target.value
        this.setState({time: value})
    }

    handleReasonChange(event) {
        const value = event.target.value
        this.setState({reason: value})
    }

    handleTechChange(event) {
        const value = event.target.value
        this.setState({tech: value})
    }

    handleVinChange(event) {
        const value = event.target.value
        this.setState({vin: value})
    }

    // handleVipChange(event) {
    //     const value = event.target.value
    //     this.setState({vip: value})
    // }


    // async handleSubmit(event){}

    async handleSubmit(event){
        event.preventDefault()
        const data = {...this.state}
        delete data.techs
        delete data.vins
        delete data.apptScheduled
        // console.log(data)

        const apptUrl = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
        },
        };
        const response = await fetch(apptUrl, fetchConfig);
        if(response.ok){
            const newAppt = await response.json();
            // console.log(newAppt);

            const cleared = {
                customer: '',
                date: '',
                time: '',
                reason: '',
                tech: '',
                vin: '',
                apptScheduled: true,
            }
            this.setState(cleared)
        }
    }

    async componentDidMount(){
        const techUrl = "http://localhost:8080/api/techs/"
        const vinUrl = "http://localhost:8080/api/inhouse-autos/"

        const response = await fetch(techUrl)
        if (response.ok) {
            const data = await response.json()
            // console.log(data)
            this.setState({techs: data.tech})
        }
        const response2 = await fetch(vinUrl)
        if (response2.ok) {
            const data2 = await response2.json()
            // console.log(data2)
            this.setState({vins: data2.inhouse_autos})
        }

    }


    render(){
        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.apptScheduled) {
          messageClasses = 'alert alert-success mb-0';
          formClasses = 'd-none';
        }
        return(
            <div className="my-5 container">
                <div className="row">
                    <div className="col col-md-auto">
                        <div className="col">
                            <div className="card shadow">
                                <div className="card-body">
                                    <h1 className="card-title">Create Appointment</h1>
                                    <form className={formClasses} onSubmit = {this.handleSubmit} id="create-appointment-form">
                                        <div className="mb-2">
                                            <input onChange={this.handleCustomerChange} value={this.state.customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                                            <label htmlFor="customer">Customer Name</label>
                                        </div>
                                        <div className="mb-2">
                                            <input onChange={this.handleDateChange} value={this.state.date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                                            <label htmlFor="date">Date</label>
                                        </div>
                                        <div className="mb-2">
                                            <input onChange={this.handleTimeChange} value={this.state.time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                                            <label htmlFor="time">Time</label>
                                        </div>
                                        <div className="mb-2">
                                            <input onChange={this.handleReasonChange} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                                            <label htmlFor="reason">Reason for Appointment</label>
                                        </div>
                                        <div className="mb-2">
                                            <select onChange={this.handleTechChange} required id="tech" name="tech" value={this.state.tech}>
                                                <option value="">Choose a tech</option>
                                                {this.state.techs.map(tech => {
                                                    return (
                                                        <option key={tech.employee_id} value={tech.name}>{tech.name}</option>
                                                    )
                                                })}
                                            </select>
                                        </div>
                                        <div className="mb-3">
                                            <select onChange={this.handleVinChange} required id="vin" name="vin" value={this.state.vin}>
                                                <option value="">Inhouse VIN</option>
                                                {this.state.vins.map(vin => {
                                                    return (
                                                        <option key={vin.vin} value={vin.vin}>{vin.vin}</option>
                                                    )
                                                })}
                                            </select>
                                        </div>
                                        <button>Create</button>
                                    </form>
                                    <div className={messageClasses} id="success-message">
                                        Appointment confirmed!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
export default AppointmentForm;
