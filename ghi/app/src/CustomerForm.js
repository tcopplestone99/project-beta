import React from "react"


class CustomerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            address: "",
            phone_number: "",
            successful: false
        }
        this.handleAddressChange = this.handleAddressChange.bind(this)
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        // this.renderSuccess = this.renderSuccess.bind(this)
    }

    handleNameChange(event) {
        const value = event.target.value
        this.setState({name: value})
        // console.log(this.state)
    }

    handleAddressChange(event) {
        const value = event.target.value
        this.setState({address: value})
    }

    handlePhoneNumberChange(event) {
        const value = event.target.value
        this.setState({phone_number: value})
    }

    renderSuccess() {
        if (this.state.successful) {
            return(
                <p>You have successfully created a customer</p>
            )
        }
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.successful
        // console.log(data)
        const custUrl = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const r = await fetch(custUrl, fetchConfig)
        if(r.ok) {
            const newCust = await r.json()
            // console.log(newCust)
            const wipe = {
                name: "",
                address: "",
                phone_number: "",
                successful: true
            }
            this.setState(wipe)
        }
    }

    render() {
        return (
            <div>
                <h1>Create a new Customer</h1>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <input onChange={this.handleNameChange} className="form-control" value={this.state.name} placeholder="Name" required type="text" name="name" id="name"/>
                        {/* <label htmlFor="name">Name</label> */}
                    </div>
                    <div className="form-group">
                        <input onChange={this.handleAddressChange} className="form-control" value={this.state.address} placeholder="Address" required type="text" name="address" id="address"/>
                        {/* <label htmlFor="address">Address</label> */}
                    </div>
                    <div className="form-group">
                        <input onChange={this.handlePhoneNumberChange} className="form-control" value={this.state.phone_number} placeholder="Phone Number" required type="text" name="phoneNumber" id="phoneNumber"/>
                        {/* <label htmlFor="phoneNumber">Phone Number</label> */}
                    </div>
                    <button>Create</button>
                    {this.renderSuccess()}
                </form>
            </div>
        )
    }




}


export default CustomerForm
