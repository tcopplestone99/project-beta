import React from "react";

class ManufacturerList extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            manufacturers: []
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/manufacturers/"
        const r = await fetch(url)
        if (r.ok) {
            const data = await r.json()
            this.setState({manufacturers: data.manufacturers})
            // console.log(this.state)
        }
    }

    render() {
        return (
            <div>
                <h1>Manufacturers</h1>
                <table className="table table-striped">
                    <tbody>
                        {this.state.manufacturers.map(m => {
                            return (
                                <tr key={m.id}>
                                    <td>{m.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }

}

export default ManufacturerList
