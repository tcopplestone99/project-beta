import React, { Component } from "react";


class AutomobileList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            autmobiles: []
        }
    }
    async componentDidMount() {
        const url = "http://localhost:8100/api/automobiles/"
        const r = await fetch(url)
        if (r.ok) {
            const data = await r.json()
            // console.log(data)
            this.setState({autmobiles: data.autos})
            // console.log(this.state)
        }
    }

    render() {
        return (
            <div className="container">
                <h1>Automobiles</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Color</th>
                            <th>Model</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                            <th>Year</th>
                            <th>Vin</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.autmobiles.map(a => {
                            return (
                                <tr key={a.id}>
                                    <td>{a.color}</td>
                                    <td>{a.model.name}</td>
                                    <td>{a.model.manufacturer.name}</td>
                                    <td>
                                    <img style={{ width:100 }} src={a.model.picture_url} alt=""></img>
                                    </td>
                                    <td>{a.year}</td>
                                    <td>{a.vin}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default AutomobileList
