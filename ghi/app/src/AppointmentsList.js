import React from "react"

class AppointmentsList extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            appointments: [],
        }
    }

    async componentDidMount(){
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url)
        if (response.ok){
            const data = await response.json()
            this.setState({appointments: data.appointments})
            // console.log("state", this.state)
        }
    }

    async deleteAppointment(appointment){
        const apptUrl = `http://localhost:8080/api/appointments/${appointment.id}`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(apptUrl, fetchConfig)
        if (response.ok) {
            const remAppt = this.state.appointments.filter((i) => appointment.id !== i.id)
            this.setState({appointments: remAppt})
        }
    }

    hideRow(appointment){
        const cleared = this.state.appointments.filter((i) => appointment.id !== i.id)
        this.setState({appointments: cleared})
    }

    render(){

        return(
            <div className="container">
                <h1>Appointments List</h1>
                <table className="table table-hover table-bordered">
                    <thead className="table-light">
                        <tr>
                            <th>VIN</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Reason</th>
                            <th>Technician</th>
                            <th>VIP Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => {
                            return(
                                <>
                                <tr key={appointment.id}>
                                    <td>{appointment.vin.vin}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.tech.name}</td>
                                    <td>{appointment.vin.vip.toString()}</td>
                                    <td>
                                        <button type="button" className="btn btn-danger" onClick={() => this.deleteAppointment(appointment)}>Delete</button>
                                    </td>
                                    <td>
                                    <button type="button" className="btn btn-danger" onClick={() => this.hideRow(appointment.id)}>Hide</button>
                                    </td>
                                </tr>
                                </>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default AppointmentsList;
