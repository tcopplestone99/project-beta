import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/customers/new">New Customer</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/salespersons/new/">New Salesperson</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/salesrecords/">Sales Records</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/salesrecords/new/">New Sales Record</NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Service</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/serviceappointments">Appointments List</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/serviceappointments/new/">New Appointment</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/servicehistory">Service History</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/technician/new/">New Technician</NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Inventory</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/">Manufacturers</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/new/">New Manufacturer</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/vehiclemodels/">Vehicle Models</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/vehiclemodels/new/">New Vehicle Model</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/automobiles/">Automobiles</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/automobiles/new/">New Automobile</NavLink>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
