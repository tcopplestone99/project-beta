import React from "react";

class VehicleModelForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            picture_url: "",
            manufacturer_id: "",
            manufacturers: [],
            successful: false
        }
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this)
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }

    renderSuccess() {
        if (this.state.successful) {
            return(
                <p>You have successfully created a vehicle model</p>
            )
        }
    }

    handleNameChange(event) {
        const value = event.target.value
        this.setState({name: value})
    }
    handlePictureUrlChange(event) {
        const value = event.target.value
        this.setState({picture_url: value})
    }
    handleManufacturerChange(event) {
        const value = event.target.value
        this.setState({manufacturer_id: value})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.manufacturers
        delete data.successful
        const url = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const r = await fetch(url, fetchConfig)
        if(r.ok) {
            const newMod = await r.json()
        }
        const wipe = {
            name: "",
            picture_url: "",
            manufacturer_id: "",
            successful: true
        }
        this.setState(wipe)
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/manufacturers/"
        const r = await fetch(url)
        if (r.ok) {
            const data = await r.json()
            this.setState({manufacturers: data.manufacturers})
        }
        // console.log(this.state)
    }

    render(){
        return (
            <div>
                <h1>Create a new vehicle model</h1>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" value={this.state.name}/>
                    </div>
                    <div>
                        <input onChange={this.handlePictureUrlChange} placeholder="Picture Url" required type="text" name="pictureUrl" id="pictureUrl" value={this.state.picture_url}/>
                    </div>
                    <div>
                        <select onChange={this.handleManufacturerChange} required id="manufacturer" name="manufacturer" value={this.state.manufacturer_id}>
                            <option value="">Choose a manufacturer</option>
                            {this.state.manufacturers.map(m => {
                                return (
                                    <option key={m.id} value={m.id}>{m.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <button>Create</button>
                </form>
                {this.renderSuccess()}
            </div>
        )
    }
}


export default VehicleModelForm
