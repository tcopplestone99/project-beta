import React from "react"

class VehicleServiceHistory extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            vin: "",
            vins: [],
            hist: "",
            hist: [],
        }
        //handles
        this.handleVinChange = this.handleVinChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleVinChange(event) {
        const value = event.target.value
        this.setState({vin: value})
    }


    async handleSubmit(event){
        event.preventDefault()
        const data = {...this.state}
        const histUrl = `http://localhost:8080/api/service-history/${this.state.vin}/`
        const response = await fetch(histUrl)
        if(response.ok){
            const data2 = await response.json()
            this.setState({hist: data2["hist"]}, function() {
                this.showHistory()
            });
        }
    }


    showHistory(){
        let result = this.state.hist.map(hi => {
            return(
                <tr key={hi.id}>
                    <td>{hi.vin.vin}</td>
                    <td>{hi.customer}</td>
                    <td>{hi.date}</td>
                    <td>{hi.time}</td>
                    <td>{hi.reason}</td>
                    <td>{hi.tech.name}</td>
                    <td>{hi.vin.vip.toString()}</td>
                </tr>
            )
        })
        return result
    }

    async componentDidMount(){
        const vinUrl = "http://localhost:8080/api/inhouse-autos/"

        const response = await fetch(vinUrl)
        if (response.ok) {
            const data = await response.json()
            // console.log(data)
            this.setState({vins: data.inhouse_autos})
        }
    }

    render(){
        return(
            <>
            <div>
                <h1>Search Vehicle Service History</h1>
                <form onSubmit={this.handleSubmit}>
                    <input onChange={this.handleVinChange} required id="vin" name="vin" value={this.state.vin}></input>
                    <label htmlFor="vin"></label>
                    <button>Search</button>
                </form>
            </div>

            <div>
            <h1>Past Appointments</h1>
            <table className="table table-hover table-bordered">
                <thead className="table-light">
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>Technician</th>
                        <th>VIP Status</th>
                    </tr>
                </thead>
                <tbody>
                    {this.showHistory(this.state.hist)}
                </tbody>
            </table>
        </div>
        </>
        )
    }
}
export default VehicleServiceHistory;
