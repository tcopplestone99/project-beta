from django.urls import path

from .views import (
    api_automobile_vo_list,
    api_customers,
    api_sales_person_details,
    api_sales_persons,
    api_sales_records
)


urlpatterns = [
    path("customers/", api_customers, name="api_list_customers"),
    path("salespersons/", api_sales_persons, name="api_list_sales_persons"),
    path("automobileVOs/", api_automobile_vo_list, name="api_list_automobleVOs"),
    path("salesrecords/", api_sales_records, name="api_list_sales_records"),
    path("salespersons/<int:pk>/", api_sales_person_details, name="api_detail_sales_person")
]
