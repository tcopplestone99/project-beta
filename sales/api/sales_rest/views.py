from django.shortcuts import render
from .models import AutomobileVO, Customer, SalesPerson, SalesRecord
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

# Create your views here.

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number", "id"]

class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id"]

class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "id"]

class SalesRecordListEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "sales_person",
        "customer",
        "price",
        "id"
    ]
    encoders = {
        "sales_person": SalesPersonListEncoder(),
        "customer": CustomerListEncoder(),

    }

    def get_extra_data(self, o):
        return {
            "vin": o.car.vin
        }

@require_http_methods(["GET"])
def api_automobile_vo_list(request):
    if request.method == "GET":
        cars = AutomobileVO.objects.all()
        return JsonResponse(
            {"cars": cars},
            encoder=AutomobileVOListEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def api_sales_persons(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonListEncoder
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonListEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def api_sales_records(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            content["customer"] = Customer.objects.get(pk=content["customer"])
            content["sales_person"] = SalesPerson.objects.get(pk=content["sales_person"])
            content["car"] = AutomobileVO.objects.get(vin=content["car"])
            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                {"sales_record": sales_record},
                encoder=SalesRecordListEncoder,
                safe=False
            )

        except:
            return JsonResponse(
                {"message": "Sales record could not be created"},
                status=400,
            )

@require_http_methods(["GET"])
def api_sales_person_details(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                {"sales_person": sales_person},
                encoder=SalesPersonListEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=404
            )
